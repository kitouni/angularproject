package com.foodtruck.FinalBackEnd.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name="Profil.findAll", query = "from Profil"),
	@NamedQuery(name="Profil.findById", query = "from Profil where id = :myID")
})
public class Profil {
	
	/*********************************************************************
	 * Properties
	 *********************************************************************/
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(nullable = false, name = "id_profil")
	private Integer id;
	private String libelle_profil;
	
	/*********************************************************************
	 * Gettes / Setters
	 *********************************************************************/
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLibelle_profil() {
		return libelle_profil;
	}
	public void setLibelle_profil(String libelle_profil) {
		this.libelle_profil = libelle_profil;
	}
	
	/*********************************************************************
	 * Constructors
	 *********************************************************************/
	
	public Profil() {
		
	}

	public Profil( String libelle_profil) {
		this.libelle_profil = libelle_profil;
	}
	

}
